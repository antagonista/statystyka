# Statystyka

Aplikacja, która potrafi generować liczby (pseudo)losowe, zapisywać / odtwarzać je oraz na podstawie ich wyliczać prostą statystykę. Napisana w C#.


## Video 
[![Watch the video](readme_files/Statystyka.png)](readme_files/Statystyka.mp4)
