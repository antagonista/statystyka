﻿namespace Statystyka
{
    partial class Losowanie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.minV = new System.Windows.Forms.TextBox();
            this.maxV = new System.Windows.Forms.TextBox();
            this.prec = new System.Windows.Forms.TextBox();
            this.ile = new System.Windows.Forms.TextBox();
            this.bLosuj = new System.Windows.Forms.Button();
            this.bAnuluj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Minimum";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Maximum";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Precyzja";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ilość";
            // 
            // minV
            // 
            this.minV.Location = new System.Drawing.Point(68, 5);
            this.minV.Name = "minV";
            this.minV.Size = new System.Drawing.Size(100, 20);
            this.minV.TabIndex = 4;
            this.minV.Text = "0";
            // 
            // maxV
            // 
            this.maxV.Location = new System.Drawing.Point(68, 35);
            this.maxV.Name = "maxV";
            this.maxV.Size = new System.Drawing.Size(100, 20);
            this.maxV.TabIndex = 5;
            this.maxV.Text = "100";
            // 
            // prec
            // 
            this.prec.Location = new System.Drawing.Point(67, 59);
            this.prec.Name = "prec";
            this.prec.Size = new System.Drawing.Size(100, 20);
            this.prec.TabIndex = 6;
            this.prec.Text = "1";
            // 
            // ile
            // 
            this.ile.Location = new System.Drawing.Point(67, 90);
            this.ile.Name = "ile";
            this.ile.Size = new System.Drawing.Size(100, 20);
            this.ile.TabIndex = 7;
            this.ile.Text = "50";
            // 
            // bLosuj
            // 
            this.bLosuj.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.bLosuj.Location = new System.Drawing.Point(16, 116);
            this.bLosuj.Name = "bLosuj";
            this.bLosuj.Size = new System.Drawing.Size(74, 23);
            this.bLosuj.TabIndex = 8;
            this.bLosuj.Text = "Losuj";
            this.bLosuj.UseVisualStyleBackColor = false;
            this.bLosuj.Click += new System.EventHandler(this.bLosuj_Click);
            // 
            // bAnuluj
            // 
            this.bAnuluj.BackColor = System.Drawing.Color.Tomato;
            this.bAnuluj.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bAnuluj.Location = new System.Drawing.Point(93, 115);
            this.bAnuluj.Name = "bAnuluj";
            this.bAnuluj.Size = new System.Drawing.Size(74, 23);
            this.bAnuluj.TabIndex = 9;
            this.bAnuluj.Text = "Anuluj";
            this.bAnuluj.UseVisualStyleBackColor = false;
            this.bAnuluj.Click += new System.EventHandler(this.bAnuluj_Click);
            // 
            // Losowanie
            // 
            this.AcceptButton = this.bLosuj;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.bAnuluj;
            this.ClientSize = new System.Drawing.Size(182, 151);
            this.Controls.Add(this.bAnuluj);
            this.Controls.Add(this.bLosuj);
            this.Controls.Add(this.ile);
            this.Controls.Add(this.prec);
            this.Controls.Add(this.maxV);
            this.Controls.Add(this.minV);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Losowanie";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Losowanie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox minV;
        private System.Windows.Forms.TextBox maxV;
        private System.Windows.Forms.TextBox prec;
        private System.Windows.Forms.TextBox ile;
        private System.Windows.Forms.Button bLosuj;
        private System.Windows.Forms.Button bAnuluj;
    }
}