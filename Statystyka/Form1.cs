﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Statystyka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            we.Clear();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (sd.ShowDialog() == DialogResult.OK)
            {
                we.SaveFile(sd.FileName, RichTextBoxStreamType.PlainText);
            }
                

                
                
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (od.ShowDialog() == DialogResult.OK)
            {
                we.LoadFile(od.FileName, RichTextBoxStreamType.PlainText);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Losowanie los = new Losowanie();
            los.ShowDialog();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {

            int c = we.Lines.Length;

            if (c == 0)
            {

                wy.Text = "Brak danych";

                return;

            }

            Stats s = new Stats(c, we.Lines);

            wy.Text = System.String.Format(

                "Znaleziono {0} wierszy z liczbami\n", s.Ile);

            wy.Text = System.String.Concat(wy.Text,

              System.String.Format("Minimum: {0}\n", s.Min()));

            wy.Text = System.String.Concat(wy.Text,

              System.String.Format("Maksimum: {0}\n", s.Maks()));

            wy.Text = System.String.Concat(wy.Text,

              System.String.Format("Zakres: {0}\n", s.Zakres()));

            wy.Text = System.String.Concat(wy.Text,

              System.String.Format("Dominanta: {0}\n", s.Dominanta()));

            wy.Text = System.String.Concat(wy.Text,

              System.String.Format("Mediana: {0}\n", s.Mediana()));

            wy.Text = System.String.Concat(wy.Text,

              System.String.Format("Średnia: {0}\n", s.Srednia()));

            wy.Text = System.String.Concat(wy.Text,

              System.String.Format(

"Odchylenie standardowe: {0}\n", s.Odchylenie()));



        }
    }
}
