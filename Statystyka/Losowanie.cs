﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Statystyka
{
    public partial class Losowanie : Form
    {
        public Losowanie()
        {
            InitializeComponent();
        }

        private void bAnuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bLosuj_Click(object sender, EventArgs e)
        {
            double lo = System.Convert.ToDouble(minV.Text);

            double hi = System.Convert.ToDouble(maxV.Text);



            int pr = System.Convert.ToInt32(prec.Text);

            int scale = 1;

            for (int n = 0; n < pr; n++) scale *= 10;

            int rng = (int)(scale * (hi - lo));



            int cnt = System.Convert.ToInt32(ile.Text);



            Random r = new Random();

            String t = "";



            for (int n = 0; n < cnt; n++)
            {

                int x = r.Next(rng);

                double y = x;

                y = y / scale + lo;

                t = System.String.Concat(t,

                    System.String.Format("{0}\n", y));

            }

            Form1 f1 = (Form1)(Application.OpenForms["Form1"]);

            f1.we.Text = System.String.Concat(f1.we.Text, t);

            Close();
        }
    }
}
