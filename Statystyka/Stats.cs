﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Statystyka
{
    class Stats
    {

        int ile;  // liczba wartości

        double[] dane;  // tablica wartości

        public int Ile
        {     // udostepnienie liczby wartości poza klasę

            get
            {

                return ile;

            }

        }

        public Stats(int c, string[] t)
        {

            dane = new double[c];

            ile = 0;

            for (int n = 0; n < c; n++)
            {

                try
                {

                    double x = System.Convert.ToDouble(t[n]);

                    dane[ile++] = x;

                }
                catch (System.OverflowException)
                {

                }
                catch (System.FormatException)
                {

                }
                catch (System.ArgumentException) { }

            }

            Array.Resize(ref dane, ile);

            Array.Sort(dane);

        }


        public double Maks()
        {

            return dane[ile - 1];

        }


        public double Min()
        {

            return dane[0];

        }


        public double Zakres()
        {

            return Maks() - Min();

        }

        public double Srednia()
        {

            double m = dane[0];

            int n = 1;

            while (n < ile)

                m += dane[n++];

            return m / ile;

        }


        public double Odchylenie()
        {

            double m = Srednia();

            double d, s = 0;

            int c = ile;

            while (--c >= 0)
            {

                d = dane[c] - m;

                s += d * d;

            }

            return Math.Sqrt(s / ile);

        }

        public double Dominanta()
        {

            int c = ile - 1;

            double v = dane[c];

            double bv = v;

            int n = 1;

            int bn = n;



            while (c-- > 0)
            {

                if (dane[c] == v)

                    n++;

                else
                {

                    if (n > bn)
                    {

                        bn = n;

                        bv = v;

                    }

                    n = 1;

                    v = dane[c];

                }

            }

            return bv;

        }

        public double Mediana()
        {
            double m = dane[ile / 2];

            if ((ile & 1) == 0)
            {

                m += dane[ile / 2 - 1];

                m /= 2;

            }

            return m;

        }


    }
}
